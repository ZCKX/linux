#include <iostream>

using namespace std;

//抽象产品类
class shoes
{
public:
    virtual ~shoes() {} 
    virtual void Show() = 0;
};

//具体产品类
class NikeShoes : public shoes
{
public:
    void Show()
    {
        cout << "我是耐克球鞋，我的广告语：Just do it" << endl;
    }
};

class AdidasShoes : public shoes
{
public:
    void Show()
    {
        cout << "我是阿迪达斯球鞋，我的广告语:Impossible is nothing" << endl;
    }
};

class LiNingShoes : public shoes
{
public:
    void Show()
    {
        cout << "我是李宁球鞋，我的广告语：Everything is possible" << endl;
    }
};

enum SHOES_TYPE
{
    NIKE,
    ADIDAS,
    LINING
};

//工厂类
class ShoesFactory
{
public:
    shoes* CreateShoes(SHOES_TYPE type)
    {
        switch(type)
        {
        case NIKE:
            return new NikeShoes();  //生产Nike鞋
            break;
        case ADIDAS:
            return new AdidasShoes(); //生产Adidas鞋
            break;
        case LINING:
            return new LiNingShoes(); //生产李宁鞋子
            break;

        }
    }

};