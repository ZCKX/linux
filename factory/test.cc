#include "factory_method.hpp"

// void test1()
// {
//     ShoesFactory shoesfactory;

//     shoes* pNikeShoes = shoesfactory.CreateShoes(NIKE);
//     pNikeShoes->Show();
//     delete pNikeShoes;

//     shoes* pAdidasShoes = shoesfactory.CreateShoes(ADIDAS);
//     pAdidasShoes->Show();
//     delete pAdidasShoes;

//     shoes* pLiNingShoes = shoesfactory.CreateShoes(LINING);
//     pLiNingShoes->Show();
//     delete pLiNingShoes;
// }

void test2()
{
    // 鞋厂开设Nike生产线
    ShoesFactory* nikeProducer = new NikeProducer();
    // 生产鞋
    shoes* nikeShoes = nikeProducer->CreateShoes();
    // 广告
    nikeShoes->Show();
    delete nikeProducer;
    delete nikeShoes;

    // 鞋厂开设Adidas生产线
    ShoesFactory* adidasProducer = new AdidasProducer();
    // 生产鞋
    shoes* adidasShoes = adidasProducer->CreateShoes();
    // 广告
    adidasShoes->Show();
    delete adidasProducer;
    delete adidasShoes;

    // 鞋厂开设LiNing生产线
    ShoesFactory* LNProducer = new LiNingProducer();
    // 生产鞋
    shoes* LNShoes = LNProducer->CreateShoes();
    // 广告
    LNShoes->Show();
    delete LNProducer;
    delete LNShoes;
}

int main()
{ 
    //test1();
    test2();
    
    return 0;
}