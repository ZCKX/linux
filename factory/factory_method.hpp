#include <iostream>

using namespace std;

//抽象产品类
class shoes
{
public:
    virtual ~shoes() {} 
    virtual void Show() = 0;
};

//具体产品类
class NikeShoes : public shoes
{
public:
    void Show()
    {
        cout << "我是耐克球鞋，我的广告语：Just do it" << endl;
    }
};

//具体产品类
class AdidasShoes : public shoes
{
public:
    void Show()
    {
        cout << "我是阿迪达斯球鞋，我的广告语:Impossible is nothing" << endl;
    }
};

//具体产品类
class LiNingShoes : public shoes
{
public:
    void Show()
    {
        cout << "我是李宁球鞋，我的广告语：Everything is possible" << endl;
    }
};

//抽象工厂类
class ShoesFactory
{
public:
    virtual shoes* CreateShoes() = 0;
    virtual ~ShoesFactory() {}
};

//具体工厂类
class NikeProducer : public ShoesFactory
{
public:
    shoes* CreateShoes()
    {
        return new NikeShoes();
    }
};

//具体工厂类
class AdidasProducer : public ShoesFactory
{
public:
    shoes* CreateShoes()
    {
        return new AdidasShoes();
    }
};

//具体工厂类
class LiNingProducer : public ShoesFactory
{
public:
    shoes* CreateShoes()
    {
        return new LiNingShoes();
    }
};