#include <iostream>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>


#include "Log.hpp"

static void Usage(const std::string porc)
{
    std::cout << "Usage:\n\t" << porc << " port [ip]" << std::endl;
}

/// @brief  我们想写一个简单的udpSever
/// 云服务器有一些特殊情况：
/// 1. 禁止你bind云服务器上的任何确定IP， 只能使用INADDR_ANY，如果你是虚拟机，随意

class UdpServer
{
public:
    UdpServer(int port, std::string ip = "")
    :sockfd_(-1)
    ,port_(port)
    ,ip_(ip)
    {

    }

    ~UdpServer()
    {

    }

public:
    void init()
    {
        // 1. 创建套接字
        // domain -- 域
        // type -- 套接字类型，SOCK_DGRAM--数据报格式
        // protocol -- 协议类型，网络应用中：0
        // 返回值其实是文件描述符
        sockfd_ = socket(AF_INET, SOCK_DGRAM, 0);  //就是打开一个文件

         if(sockfd_ < 0)
         {
            logMessage(FATAL, "socket:%s:%d", strerror(errno), sockfd_);
            exit(SOCK_ERR);
         }
         logMessage(DEBUG, "socket create success:%d", sockfd_);

        // 2. 绑定网络信息，指明ip+port
        // 2.1 先填充基本信息到 struct sockaddr_in
        struct sockaddr_in local;
        bzero(&local, sizeof(local));  //初始化为0
        local.sin_family = AF_INET;  //填充协议家族，域
        local.sin_port = htons(port_);     //填充服务对应的端口号信息,一定会发给对方，因此port_一定回到网络中
        //local.sin_addr;
        local.sin_addr.s_addr = ip_.empty()? htonl(INADDR_ANY) : inet_addr(ip_.c_str());  // htonl将INADDR_ANY转换成32位网络字节序， inet_addr将字符串中的点分十进制转换成32位比特位

        if(bind(sockfd_, (const struct sockaddr*)&local, sizeof(local)) == -1)
        {
            logMessage(FATAL, "bind:%s", strerror(errno));
            exit(BIND_ERR);
        }
        logMessage(DEBUG, "socket bind success:%d", sockfd_);
        //完成
    }

    void start()
    {
        //  服务器都是在一个死循环当中
        char inbuffer[1024];  //将来读取到的数据，都放在这里
        char outbuffer[1024]; //将来发送的数据，都放在这里
        while(true)
        {
            memset(inbuffer, 0, sizeof(inbuffer));
            struct sockaddr_in peer;      //输出型参数
            socklen_t len = sizeof(peer); //输入输出型参数
            // UDP是无连接的
            // 对方发消息，你需要接收消息
            ssize_t s = recvfrom(sockfd_, inbuffer, sizeof(inbuffer) - 1, 0, \
                (struct sockaddr*)&peer, &len);
            logMessage(DEBUG, "server 提供服务中.....");
            if(s > 0) 
            {
                //接收成功
                inbuffer[s] = '\0';
            }
            else if(s == -1)
            {
                //
                logMessage(WARINING, "recvfrom fialed:%s[%d]", strerror(errno), sockfd_);
                continue;
            }
            // 读取成功的,除了读取到对方的数据，你还要读取到对方的网络地址[ip:port]
            std::string peerIp = inet_ntoa(peer.sin_addr);  //拿到对方的ip
            uint32_t peerPort = ntohs(peer.sin_port);  //拿到对方的port
            // 打印出来客户端给服务器发送过来的消息
            logMessage(NOTICE, "[%s:%d]# %s", peerIp.c_str(), peerPort, inbuffer);
            for(int i = 0; inbuffer[i] != '\0'; ++i)
            {
                if(isalpha(inbuffer[i]) > 0)
                {
                    inbuffer[i] = toupper(inbuffer[i]);
                }
            }
            sendto(sockfd_, inbuffer, sizeof(inbuffer) - 1, 0,\
                (const struct sockaddr*)&peer, sizeof(peer));
            logMessage(DEBUG, "server 提供服务中.....");
            //sleep(1);
        }
    }
private:
    // 服务器的fd
    int sockfd_;
    // 服务器必须得有端口号信息
    uint16_t port_;
    // 服务器必须得有IP地址
    std::string ip_;
};

// ./server port ip
int main(int argc, char *argv[])
{
    if(argc != 2 && argc != 3) 
    {
        Usage(argv[0]);
        exit(PARA_ERR);
    }

    uint16_t port = atoi(argv[1]); 
    std::string ip;
    if(argc == 3)
    {
        ip = argv[2];
    }
    UdpServer server(port,ip);
    server.init();
    server.start();

    return 0;
}