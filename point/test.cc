#include "ptr.hpp"

//template<class T>
void func(zch::shared_ptr<int>& sp, int n)
{
    for (int i = 0; i < n; ++i)
    {
        zch::shared_ptr<int> copy(sp);
    }
}

int main()
{
    zch::shared_ptr<int> p(new int(10));

    /*thread t1(func, ref(p), 1000);
    thread t2(func, ref(p), 1000);*/
    thread t1(func, ref(p), 1000);
    thread t2(func, ref(p), 1000);
    //thread t2(func, p, 1000);
    //func(p, 1000);
    t1.join();
    t2.join();
    
    cout << p.use_count() << endl;
    return 0;
}