#include <stdio.h>
#include <unistd.h>

int main()
{
    pid_t id = fork();
    if(id == 0)
    {
        //子进程
        while(1)
        {
            printf("这是子进程，pid: %d, ppid: %d, id: %d\n", getpid(), getppid(), id);
            sleep(1);
        }
    }
    else if(id > 0)
    {
        //父进程
        while(1)
        {
            printf("这是父进程，pid: %d, ppid: %d, id: %d\n", getpid(), getppid(), id);
            sleep(2);
        }
    }
    else 
    {
        perror("fork failed");
    }
    return 0;
}
