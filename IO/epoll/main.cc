#include <iostream>
#include <memory>
#include <sys/select.h>

#include "Sock.hpp"
#include "Epollserver.hpp"

using namespace std;

void user(std::string process)
{
    cout << "\nUsage: " << process << " port" << endl;
}

int myfunc(int sock)
{
    //bug
    char buffer[1024];
    ssize_t s = recv(sock, buffer, sizeof(buffer)-1, 0); //不会被阻塞
    if(s > 0)
    {
        buffer[s] = 0;
        logMessage(DEBUG, "client[%d]# %s", sock, buffer);
    }
    return s;
}

// ./SelectServer 8080
int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        user(argv[0]);
        exit(USAGE_ERR);
    }
    unique_ptr<EpollServer> epollserver(new EpollServer(atoi(argv[1]), &myfunc));
    epollserver->InitEpollServer();
    epollserver->Run();

    return 0;
}