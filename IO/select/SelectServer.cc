#include <iostream>
#include <sys/select.h>
#include "Sock.hpp"

#define DFL -1

static int fdsArray[sizeof(fd_set) * 8] = {0}; // 保存历史上所有的合法fd
int gnum = sizeof(fdsArray) / sizeof(fdsArray[0]);

using namespace std;

void user(std::string process)
{
    cout << "\nUsage: " << process << " port" << endl;
}

static void showArray(int arr[], int num)
{
    cout << "当前合法sock list# ";
    for (int i = 0; i < num; i++)
    {
        if (arr[i] == DFL)
            continue;
        else
            cout << arr[i] << " ";
    }
    cout << endl;
}

// readfds: 现在包含就是已经就绪的sock
static void HandlerEvent(int listensock, fd_set &readfds)
{
    for (int n = 0; n < gnum; ++n)
    {
        if (fdsArray[n] == DFL)
            continue;

        if (n == 0 && fdsArray[n] == listensock)
        {
            if (FD_ISSET(listensock, &readfds))
            {
                // 具有了一个新链接
                cout << "已经有一个新链接到来了，需要进行获取(读取/拷贝)了" << endl;
                string clientip;
                uint16_t clientport = 0;
                int sockfd = sock::Accept(listensock, &clientip, &clientport);
                if (sockfd < 0)
                {
                    // 获取链接失败
                    logMessage(WARINING, "accept: %s[%d]", strerror(errno), sockfd);
                    exit(CONN_ERR);
                }
                cout << "clientip: " << clientip << " clientport: " << clientport << endl;

                // read/write -- 不能，因为你read不知道底层数据是否就绪！！select知道！
                // 想办法把新的fd托管给select？如何托管？？
                int i = 0;
                for (; i < gnum; ++i)
                {
                    if (fdsArray[i] == DFL)
                        break;
                }

                if (i == gnum)
                {
                    cout << "服务器已经到了最大上限，无法再承载更多同时保持的连接了" << endl;
                    close(sockfd);
                }
                else
                {
                    fdsArray[i] = sockfd; // 将sock添加到select中，进行进一步的监听就绪事件了！
                }
            }
        } // end if (n == 0 && fdsArray[n] == listensock)
        else
        {
            if (FD_ISSET(fdsArray[n], &readfds))
            {
                // 一定是一个合法的普通的IO类sock就绪了
                // read/recv读取即可
                // TODO bug
                char buffer[1024];
                ssize_t s = recv(fdsArray[n], buffer, sizeof(buffer) - 1, 0);
                if (s > 0)
                {
                    buffer[s] = 0;
                    cout << "client[" << fdsArray[n] << "]# " << buffer << endl;
                }
                else if (s == 0)
                {
                    cout << "client[" << fdsArray[n] << "] quit, server close " << fdsArray[n] << endl;
                    close(fdsArray[n]);
                    fdsArray[n] = DFL; // 去除对该文件描述符的select事件监听
                    showArray(fdsArray, gnum);
                }
                else
                {
                    cout << "client[" << fdsArray[n] << "] error, server close " << fdsArray[n] << endl;
                    close(fdsArray[n]);
                    fdsArray[n] = DFL; // 去除对该文件描述符的select事件监听
                    showArray(fdsArray, gnum);
                }
            }
        }
    }
}

// ./SelectServer 8080
int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        user(argv[0]);
        exit(USAGE_ERR);
    }

    int listensock = sock::Socket();
    sock::Bind(listensock, atoi(argv[1]));
    sock::Listen(listensock);

    for (int i = 0; i < gnum; ++i)
        fdsArray[i] = DFL;
    fdsArray[0] = listensock;

    while (true)
    {
        // 在每次进行select的时候进行我们的参数重新设定
        int maxFd = DFL;
        fd_set readfds;
        FD_ZERO(&readfds);
        // readfds是一种类型，位图类型，能定义变量，那么就一定有大小，就一定有上限
        // fd_set fds; // fd_set是用位图表示多个fd的
        // cout << sizeof(fds) * 8 << endl;  //1024

        struct timeval timeout = {5, 0};
        for (int i = 0; i < gnum; ++i)
        {
            if (fdsArray[i] == DFL)
                continue; // 1. 过滤不合法的fd

            FD_SET(fdsArray[i], &readfds); // 2. 添加所有的合法的fd到readfds中，方便select统一进行就绪监听

            if (maxFd < fdsArray[i]) // 3. 更新出最大值
                maxFd = fdsArray[i];
        }

        // 如何看待监听socket，获取新连接的，本质需要先三次握手，前提给我发送syn -> 建立连接的本质，其实也是IO，一个建立好的
        // 连接我们称之为：读事件就绪！listensocket 只（也）需要关心读事件就绪！
        // accept: 等 + "数据拷贝"
        // int sock = Sock::Accept(listensock, );
        // 编写多路转接代码的时候，必须先保证条件就绪了，才能调用IO类函数！
        int n = select(maxFd + 1, &readfds, nullptr, nullptr, &timeout);
        switch (n)
        {
        case 0:
            cout << "time out: " << (unsigned long)time(nullptr) << endl;
            break;
        case -1:
            cerr << errno << ": " << strerror(errno) << endl;
            break;
        default:
            HandlerEvent(listensock, readfds);
            // 等待成功
            // 1. 刚启动的时候，只有一个fd，listensock
            // 2. server 运行的时候，sock才会慢慢变多
            // 3. select 使用位图，采用输出输出型参数的方式，来进行 内核<->用户 信息的传递， 每一次调用select，都需要对历史数据和sock进行重新设置！！！
            // 4. listensock，永远都要被设置进readfds中！
            // 5. select 就绪的时候，可能是listen 就绪，也可能是普通的IO sock就绪啦！！
            break;
        }
    }

    return 0;
}
