#include <iostream>
#include <string>
#include <vector>
#include <string.h>

using namespace std;

int main()
{
    string line;
    vector<string> strArr;
    getline(cin, line);
    cout << line << endl;
    
    int i = 0;
    while(i < line.size())
    {
        string str;
        int flag = 0;
        if(line[i] == ' ')
            ++i;
        while(i < line.size() && (line[i] != ' ' || (line[i] == ' ' && flag == 1)))
        {
            if(line[i] == '"') 
            {
                ++flag;
                if(flag == 2)
                    break;
                else
                    continue;
            }
            str.push_back(line[i]);
            ++i;
        }
        strArr.push_back(str);
        
    }
    
    cout << strArr.size() << endl;
    for(auto str : strArr)
    {
        cout << str << endl;
    }
     
    return 0;
}