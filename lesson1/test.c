// #include <stdio.h>
// #include <time.h>

// void Print(int val)
// {
//     long long timetmp = time(NULL);
//     printf("val = %d, timetmp: %lld\n", val, timetmp);
// }

// int GetSum(int begin, int end)
// {
//     int ret = 0;

//     for (int i = begin; i <= end; ++i)
//     {
//         ret += i;
//     }

//     return ret;
// }

// int main()
// {
//     int val = GetSum(0, 100);
//     Print(val);
//     return 0;
// }

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct _NodeInfo
{
 //序号
 int id;
 //名字 , 最大 20 字节
 char name[20];
 //Next 
 struct _NodeInfo * next;
}NODE_INFO;

NODE_INFO *GetNode(int i)
{
    NODE_INFO* tmp = (NODE_INFO*)malloc(sizeof(NODE_INFO));
    tmp->id = i;
    memset(tmp->name, 0, sizeof(tmp->name));
    sprintf(tmp->name, "%d", i);
    tmp->next = NULL;
    return tmp;
}

NODE_INFO *CreatList()
{
    NODE_INFO* head = NULL;
    NODE_INFO* cur = NULL;
    for(int i = 1; i <= 10; ++i)
    {
        if(head == NULL)
        {
            head = GetNode(i);
            cur = head;
        }
        else
        {
            cur->next = GetNode(i);
            cur = cur->next;
        }
    }
    return head;
}


 NODE_INFO * DeleteLastNode(NODE_INFO * Head)
 {
    if(Head == NULL)
    {
        return Head;
    }

    NODE_INFO* cur = Head;
    NODE_INFO* prev = NULL;
    while(cur->next)
    {
        prev = cur;
        cur = cur->next;
    }

    prev->next = NULL;
    free(cur);
    cur = NULL;
    return Head;
 }


 int main()
 {
    NODE_INFO* list = CreatList();
    NODE_INFO* cur = list;

    while(cur)
    {
        printf("%d %s\n", cur->id, cur->name);
        cur = cur->next;
    }
    list = DeleteLastNode(list);
    while(cur)
    {
        printf("%d %s\n", cur->id, cur->name);
        cur = cur->next;

    }
    return 0;
 }