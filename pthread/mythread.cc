// #include <iostream>
// #include <unistd.h>
// #include <pthread.h>


// using namespace std;

// pthread_mutex_t mutexA = PTHREAD_MUTEX_INITIALIZER;
// pthread_mutex_t mutexB = PTHREAD_MUTEX_INITIALIZER;

// void *startRoutine1(void *args)
// {
//     while (true)
//     {
//         pthread_mutex_lock(&mutexA);  //线程t1先申请mutexA
//         sleep(1);
//         pthread_mutex_lock(&mutexB);   //线程t1申请mutexB锁时该锁已被t2线程申请

//         cout << "我是线程1,我的tid: " << pthread_self() << endl;

//         pthread_mutex_unlock(&mutexA);
//         pthread_mutex_unlock(&mutexB);
//     }
// }
// void *startRoutine2(void *args)
// {
//     while (true)
//     {
//         pthread_mutex_lock(&mutexB); //线程t2先申请mutexB
//         sleep(1);
//         pthread_mutex_lock(&mutexA);//线程t2申请mutexA锁时该锁已被t1线程申请

//         cout << "我是线程2, 我的tid: " << pthread_self() << endl;

//         pthread_mutex_unlock(&mutexB);
//         pthread_mutex_unlock(&mutexA);
//     }
// }

// int main()
// {
//     pthread_t t1, t2;

//     pthread_create(&t1, nullptr, startRoutine1, nullptr);
//     pthread_create(&t2, nullptr, startRoutine2, nullptr);

//     pthread_join(t1, nullptr);
//     pthread_join(t2, nullptr);

//     return 0;
// }


#include <iostream>
#include <vector>
#include <string>
#include <functional>
#include <unistd.h>
#include <pthread.h>

using namespace std;

// 定义一个条件变量
pthread_cond_t cond;
// 定义一个互斥锁
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER; //当前不用，但是接口需要，所以我们需要留下来

vector<function<void()>> funcs;
 
void show()
{
    cout << "hello show" << endl;
}

void print()
{
    cout << "hello print" << endl;
}

// 定义全局退出变量
volatile bool quit = false;

void *waitCommand(void *args)
{
    pthread_detach(pthread_self());
    while (!quit)
    {
        // 执行了下面的代码，证明某一种条件不就绪(现在还没有场景)，要我这个线程等待
        // 三个线程，都会进在条件变量下进行排队
        pthread_cond_wait(&cond, &mutex); //让对应的线程进行等待，等待被唤醒
        for(auto &f : funcs)
        {
            f();
        }
    }
    cout << "thread id: " << pthread_self() << " end..." << endl;

    return nullptr;
}

int main()
{
    funcs.push_back(show);
    funcs.push_back(print);
    funcs.push_back([](){
        cout << "你好世界!" << endl;
    });

    pthread_cond_init(&cond, nullptr);
    pthread_t t1, t2, t3;
    pthread_create(&t1, nullptr, waitCommand, nullptr);
    pthread_create(&t2, nullptr, waitCommand, nullptr);
    pthread_create(&t3, nullptr, waitCommand, nullptr);

    while(true)
    {
        //other code, 
        sleep(1);
        pthread_cond_broadcast(&cond);
        // char n = 'a';
        // cout << "请输入你的command(n/q): ";
        // cin >> n;
        // if(n == 'n') pthread_cond_signal(&cond);
        // else break;
        // if(n == 'n') 
        // {
        //     quit = true;
        //     cout << "quit : " << quit << endl;
        //     sleep(1);

        //     pthread_cond_broadcast(&cond);
        //     break;
        // }
        // else
        // {
        //     quit = true;
        //     break;
        // }

        //sleep(1);
    }
    //sleep(1);
    cout << "main thread quit" << endl;
    //pthread_cond_broadcast(&cond);
    // pthread_cancel(t1);
    // pthread_cancel(t2);
    // pthread_cancel(t3);



    // pthread_join(t1, nullptr);
    // pthread_join(t2, nullptr);
    // pthread_join(t3, nullptr);
    pthread_cond_destroy(&cond);

    return 0;
}