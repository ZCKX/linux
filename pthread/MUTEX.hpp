#include <iostream>
#include <pthread.h>
using namespace std;

class Mutex
{
public:
    Mutex()
    {
        pthread_mutex_init(&Lock_, nullptr);
    }

    ~Mutex()
    {
        pthread_mutex_destroy(&Lock_);
    }

    void Lock()
    {
        pthread_mutex_lock(&Lock_);
    }

    void unLock()
    {
        pthread_mutex_unlock(&Lock_);
    }

private:
    pthread_mutex_t Lock_;
};

class LockGuard
{
public:
    LockGuard(Mutex *mutex)
    :mutex_(mutex)
    {
        mutex_->Lock();
        cout << "加锁成功" << endl;
    }

    ~LockGuard()
    {
        mutex_->unLock();
        cout << "解锁成功" << endl;
    }
private:
    Mutex* mutex_;
};