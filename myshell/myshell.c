#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdlib.h>

#define NUM 1024

char buff[1024];
char comment_line[1024];

int main()
{
    while(true)
    {
        printf("[用户名@主机名 当前目录]# \r\b\t");
        
        fflush(stdout);
        memset(comment_line, '\0', sizeof(comment_line));
        fgets(comment_line, NUM, stdin);
        sleep(1);
    }

    return 0;
}



