#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>

int main()
{
    printf("进程pid：%d\n", getpid());
    //execl("/usr/bin/ls", "ls", NULL);
    //execl("/usr/bin/ls", "ls", "-a", "-l", NULL);
    pid_t id = fork();

    if(id == 0)
    {
        //child
        printf("这是子进程pid：%d，ppid：%d\n", getpid(), getppid());
        //execl("/usr/bin/ls", "ls", "-al", NULL);
        char *const myenv[] = {
            (char*)"ls",
            (char*)"-a",
            (char*)"-l",
            NULL
        };
        //char *const myenv[] = {
        //    (char*)"pwd",
        //    NULL
        //};
        //char *const env_[] = {
        //    (char*)"MYPATH=HELLOWORLD",
        //    NULL
        //};

        //execve("./mytest", myenv, env_);
        //execl("./mytest", "mytest", NULL);
        //execle("./mytest", "mytest", NULL, env_);
        //execle("/home/zckx/linux/replace_process/mytest", "mytest", NULL);
        //execvp("ls", myenv);
        execv("/usr/bin/ls", myenv);
        //execv("/usr/bin/pwd", myenv);

        //execlp("ls", "ls", "-a", NULL);
        exit(1);
    }

    //parent
    printf("这是父进程pid：%d，ppid：%d\n", getpid(), getppid());
    pid_t ret = waitpid(id, NULL, 0);
    if(ret > 0) printf("子进程回收成功,ret:%d\n", ret);
    return 0;
}
