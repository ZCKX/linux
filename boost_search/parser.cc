#include <iostream>
#include <vector>
#include <string>
#include <boost/filesystem.hpp>
#include "util.hpp"

std::string src_path = "data/html";
std::string out_put = "data/raw_html/raw.txt";

typedef struct DocInfo
{
    std::string title;   //文档的标题
    std::string content; //文档的内容
    std::string url;     //文档在官网中的url
}DocInfo_t;

//const &: 输入
//*: 输出
//&：输入输出

bool EnumFile(const std::string &src_path, std::vector<std::string> *files_list);
bool ParseHtml(const std::vector<std::string> &files_list, std::vector<DocInfo_t> *results);
bool SaveHtml(const std::vector<DocInfo_t> &results,const std::string &out);



int main()
{
    //第一步: 递归式的把每个html文件名带路径，保存到files_list中，方便后期进行一个一个的文件进行读取
    std::vector<std::string> files_list;
    if(!EnumFile(src_path, &files_list))
    {
        std::cerr << "enum fail name error" << std::endl;
        return 1;
    }

    //第二步：读取files_list每个文件中的内容，并进行解析
    std::vector<DocInfo_t> doc_list;
    if(!ParseHtml(files_list, &doc_list))
    {
        std::cerr << "parse html fail" << std::endl;
        return 2;
    }

    //第三步： 把解析完毕的各个文件内容，写入到output,按照\3作为每个文档的分割符
    if(!SaveHtml(doc_list, out_put))
    {
        std::cerr << "save html fail" << std::endl;
        return 3;
    }

    //
    return 0;
}

//保存路径
bool EnumFile(const std::string &src_path, std::vector<std::string> *files_list)
{
    namespace fs = boost::filesystem;
    fs::path root_path(src_path); // 定义一个path对象
    if(!fs::exists(root_path))
    {
        std::cerr << src_path << " no exist" << std::endl;
        return false;
    }

    //std::cout << "debug: " << src_path << std::endl;

    //定义一个空的迭代器，用来判断递归结束
    fs::recursive_directory_iterator end;
    for(fs::recursive_directory_iterator it(root_path); it != end; ++it)
    {
        if(!fs::is_regular_file(*it))
        {
            continue;
        }
        
        // path()理解为提取当前路径字符串， extension()为提取字符串的后缀
        if(it->path().extension() != ".html")
        {
            continue; 
        }

        //std::cout << "debug: " << it->path().string() << std::endl;
        //当前路径文件为.html文件
        files_list->push_back(it->path().string());
    }

    return true;
}

static bool ParseTitle(const std::string &file, std::string *title)
{
    size_t begin = file.find("<title>");
    if(begin == std::string::npos)
    {
        return false;
    }

    size_t end = file.find("</title>");
    if(end == std::string::npos)
    {
        return false;
    }

    begin += std::string("<title>").size();
    if(begin > end)
    {
        return false;
    }
    std::string filetitle = file.substr(begin, end - begin);
    *title = filetitle;
    
    return true;
}

static bool ParseContent(const std::string &file, std::string *content)
{
    enum status
    {
        LABLE,
        CONTENT
    };

    status s = LABLE;
    for(char c : file)
    {
        switch(s)
        {
            case LABLE:
                if(c == '>') s = CONTENT;
                break;
            case CONTENT:
                if(c == '<') s = LABLE;
                else
                {
                    //我们不想保留原始文件中的\n,因为我们想用\n作为html解析之后文本的分隔符
                    if(c == '\n') c = ' ';
                    content->push_back(c);
                }
                break;
            default:
                break;   
        }
    }

    return true;
}

static bool ParseUrl(const std::string &file_path, std::string *url)
{
    std::string url_head = "https://www.boost.org/doc/libs/1_78_0/doc/html";
    std::string url_tail = file_path.substr(src_path.size());

    *url = url_head + url_tail;
    return true;
}

void ShowDoc(DocInfo_t &doc)
{
    std::cout << doc.title << std::endl;
    std::cout << doc.content << std::endl;
    std::cout << doc.url << std::endl;
}

//提取每个html文件的title、content、构建url
bool ParseHtml(const std::vector<std::string> &files_list, std::vector<DocInfo_t> *results)
{
    for(auto& file_path : files_list)
    {
        //1. 读取文件，Read();
        std::string res;
        if(!ns_util::FileUtil::ReadFile(file_path, &res))
        {
            continue;
        }
        
        //std::cout << "debug:" << res << std::endl; 
        DocInfo_t doc;
        //2. 解析指定的文件，提取title
        if(!ParseTitle(res, &doc.title))
        {
            continue;
        }
        
        //3. 解析指定的文件，提取content,就是去标签
        if(!ParseContent(res, &doc.content))
        {
            continue;
        }

        //4. 解析指定的文件路径，构建url
        if(!ParseUrl(file_path, &doc.url))
        {
            continue;
        }
        //ShowDoc(doc);
        //break;
        
        //done,一定是完成了解析任务，当前文档的相关结果都保存在了doc里面
        results->push_back(std::move(doc)); //bug:todo;细节，本质会发生拷贝，效率可能会比较低
    }

    return true;
}

bool SaveHtml(const std::vector<DocInfo_t> &results, const std::string &outfile)
{
#define SEP '\3'
    std::ofstream out(outfile, std::ios::out | std::ios::binary);
    if(!out.is_open())
    {
        std::cerr << outfile << "open fail" <<std::endl;
        return false;
    }

    for(auto &doc : results)
    {
        std::string out_string;
        out_string = doc.title;
        out_string += SEP;
        out_string += doc.content;
        out_string += SEP;
        out_string += doc.url;
        out_string += '\n';

        out.write(out_string.c_str(), out_string.size());
    }

    out.close();
    return true;
}
