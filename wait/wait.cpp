#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>

//using namespace std;

int main()
{
    pid_t id = fork();

    if(id == 0)
    {
        //child
        int cnt = 3;
        while(cnt--)
        {
            printf("这是子进程，pid：%d，ppid：%d，id：%d\n", getpid(), getppid(), id);
            sleep(1);
        }

        printf("子进程结束啦，父进程准备回收子进程！！！\n");
        return -1;
    }
    else if(id > 0)
    {
        //parent
        printf("这是父进程，pid：%d，ppid：%d，id：%d\n", getpid(), getppid(), id);
        sleep(5);

        pid_t ret = wait(NULL);   //父进程会停在这里等待读取子进程退出信息
        printf("回收子进程成功，子进程状态status：%d\n", ret); 
        sleep(3);
    }

    return 0;
}