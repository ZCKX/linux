#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>

//using namespace std;

int main()
{
    pid_t id = fork();

    if(id == 0)
    {
        //child
//        int cnt = 5;
//        while(cnt--)
//        {
            printf("这是子进程，pid：%d，ppid：%d，id：%d\n", getpid(), getppid(), id);
//            sleep(1);
//        }
        printf("i am child process\n");
        sleep(1);

        //printf("子进程结束啦，父进程准备回收子进程！！！\n");
        return 1;
    }
    else if(id > 0)
    {
        //parent
        int status = 0;
        printf("这是父进程，pid：%d，ppid：%d，id：%d\n", getpid(), getppid(), id);
        sleep(10);

        pid_t ret = waitpid(id, &status, 0);   //父进程会停在这里等待读取子进程退出信息
        printf("等待子进程成功, ret：%d， 子进程退出码为：%d，子进程退出信号为：%d\n", ret, (status >> 8) & 0xFF, status & 0x7f); 
        sleep(3);
    }

    return 0;
}