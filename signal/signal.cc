#include <iostream>
#include <signal.h>
#include <unistd.h>

using namespace std;

void sigcb(int signo)
{
    cout << "signo: " << signo << endl;
}

int main()
{
    signal(SIGINT, sigcb);
    int i = 1/0;    
    while(1)
    {
        cout << "父进程pid：" << getpid() << endl;
        sleep(1);
    }
    return 0;
}
